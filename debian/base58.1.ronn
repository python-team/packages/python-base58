base58(1) -- Base58 encode or decode
=================================================================================

## SYNOPSIS

`base58` [*-h*] [*-d*] [*-c*] [*FILE*]



## DESCRIPTION

Base58 encode or decode *FILE*, or standard input, to standard output.

## OPTIONS

 * `-h`, `--help` :
   show this help message and exit
 * `-d`, `--decode` :
   decode data
 * `-c`, `--check` :
   append a checksum before encoding

## COPYRIGHT

**python-base58** is copyright (c) 2013-2018 David Keijser. Released under the
MIT license.

## AUTHOR
This manual page was written by Joel Cross <joel@kazbak.co.uk\>
